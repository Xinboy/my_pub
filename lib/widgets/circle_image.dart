import 'package:flutter/material.dart';

/// 圆形图片，可定义背景色、边框属性
class CircleImageContainer extends StatelessWidget {
  final double size;
  final ImageProvider? image;
  final Color? bgColor;
  final Color? borderColor;
  final double borderWidth;

  const CircleImageContainer(
      {Key? key,
      required this.size,
      this.image,
      this.bgColor,
      this.borderColor,
      this.borderWidth = 0.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (image != null) {
      return Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
            color: bgColor ?? Colors.transparent,
            borderRadius: BorderRadius.circular(size * 0.5),
            image: DecorationImage(image: image!, fit: BoxFit.cover),
            border: Border.all(
              color: borderColor ?? Colors.transparent,
              width: borderWidth,
              style: BorderStyle.solid,
            )),
      );
    } else {
      return Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
            color: bgColor ?? Colors.transparent,
            borderRadius: BorderRadius.circular(size * 0.5),
            border: Border.all(
              color: borderColor ?? Colors.transparent,
              width: borderWidth,
              style: BorderStyle.solid,
            )),
      );
    }
  }
}

/// 圆形图片
class CircleImageClipOval extends StatelessWidget {
  final double size;
  final String imagePath;

  const CircleImageClipOval(
      {Key? key, required this.size, required this.imagePath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Image.asset(
        imagePath,
        height: size,
        width: size,
        fit: BoxFit.cover,
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:my_pub/utils/sp_utils.dart';
import 'radius_button.dart';

/// 倒计时按钮
/// 使用 SharePreference 存储时间。退回当前界面后，可以使得计数不重置
/// 使用了圆角按钮，可根据需求更改 child
class CountDownButton extends StatefulWidget {
  // 时间完成后的处理
  final Function? onTimerFinish;
  // 点击按钮的的处理
  final Function? onTimerBegin;
  // 默认显示的文本
  final String normalString;
  // 默认显示的文字颜色
  final Color normalColor;
  // 倒计时的文本（23s 重新获取）
  final String waitString;
  // 倒计时的文字颜色
  final Color waitColor;
  // 倒计时的最大值
  final int countdownTime;
  // 按钮的名字，解决离开界面后倒计时立刻重新开始的问题
  final String buttonKey;

  final double width;
  final double height;
  final BorderRadius? borderRadius;
  final Color? backgroundColor;

  const CountDownButton({
    Key? key,
    required this.buttonKey,
    this.onTimerBegin,
    this.onTimerFinish,
    this.normalString = "获取验证码",
    this.normalColor = Colors.white,
    this.waitString = "后重新获取",
    this.waitColor = const Color.fromARGB(255, 17, 132, 255),
    this.countdownTime = 60,
    this.width = 0.0,
    this.height = 0.0,
    this.borderRadius,
    this.backgroundColor,
  }) : super(key: key);

  @override
  _CountDownButtonState createState() => _CountDownButtonState();
}

class _CountDownButtonState extends State<CountDownButton> {
  Timer? _timer;

  int _currentTime = 0;

  _startCountdownTimer() {

    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      setState(() {
        _currentTime--;
      });
      if(_currentTime == 0) {
        if (widget.onTimerFinish != null) {
          widget.onTimerFinish!();
        }
        _timer?.cancel();
        _timer = null;
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState


    String beginTime = SpUtils.getString(widget.buttonKey)!;
    if (beginTime.isNotEmpty) {
      var last = DateTime.parse(beginTime);
      var sec = DateTime
          .now()
          .difference(last)
          .inSeconds;
      if (sec < widget.countdownTime) {
        //时间未到，将之前的值赋值
        setState(() {
          _currentTime = widget.countdownTime - sec;
        });
        _startCountdownTimer();
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RadiusButton(
      width: widget.width,
      height: widget.height,
      borderRadius: widget.borderRadius,
      backgroundColor: widget.backgroundColor,
      child: Text(
        _currentTime > 0
            ? "$_currentTime S $widget.waitString"
            : widget.normalString,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 14,
            color: _currentTime > 0 ? widget.waitColor : widget.normalColor),
      ),
      onPressed: () {
        // 定时器正在工作
        if (_timer != null) {
          return;
        }

        // 如果为0，说明还未开始或者已经结束
        if (_currentTime == 0) {
          _currentTime = widget.countdownTime;
        }
        if (widget.onTimerBegin != null) {
          widget.onTimerBegin!();
        }
        //存储开始时间，并启动定时器
        SpUtils.setString(widget.buttonKey, DateTime.now().toString());
        _startCountdownTimer();
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (_timer != null) {
      _timer?.cancel();
      _timer = null;
    }
  }
}

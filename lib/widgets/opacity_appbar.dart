import 'package:flutter/material.dart';

/// 自定义Appbar，支持滑动隐藏/显示
class OpacityAppBar extends StatefulWidget  implements PreferredSizeWidget {
  /// appbar 高度
  final double contentHeight;
  /// 左侧控件
  final Widget leadingWidget;
  /// 右侧控件
  final Widget? trailingWidget;
  /// 中间文字
  final String? title;
  /// 是否显示导航栏底部线条
  final bool isHiddenBottomLine;
  /// 透明度更新，在 initState 实现
  Function? updateAppBarOpacity;

  OpacityAppBar({
    Key? key,
    required this.leadingWidget,
    this.title,
    this.contentHeight = 44,
    this.trailingWidget,
    this.isHiddenBottomLine = true,
  }) : super(key: key);

  @override
  _OpacityAppBarState createState() => _OpacityAppBarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(contentHeight);
}

/// 这里没有直接用SafeArea，而是用Container包装了一层
/// 因为直接用SafeArea，会把顶部的statusBar区域留出空白
/// 外层Container会填充SafeArea，指定外层Container背景色也会覆盖原来SafeArea的颜色
/// var statusheight = MediaQuery.of(context).padding.top;  获取状态栏高度
class _OpacityAppBarState extends State<OpacityAppBar> {
  /// 当前透明度，默认不显示； 根据实际项目设置
  double opacity = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("appbar initState");
    // 外界根据滑动offset，修改透明度
    widget.updateAppBarOpacity = (double op) {
      print(op);
      setState(() {
        opacity = op;
      });
    };
  }


  /// 本案例因为 appbar 使用了背景图片，因此 Opacity 只用于appbar的子控件；
  /// 否则会造成 opacity 为0 时，appbar所在区域为空白区域
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
//      decoration: greenBgBarDecoration(),
      child: SafeArea(
        top: true,
        child: Container(
            decoration: UnderlineTabIndicator(
              borderSide: BorderSide(
                  width: 1.0,
                  color: widget.isHiddenBottomLine
                      ? const Color(0xFFeeeeee)
                      : Colors.transparent),
            ),
            height: widget.contentHeight,
            child: Opacity(
              opacity: opacity,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    left: 0,
                    child: Container(
                      padding: const EdgeInsets.only(left: 5),
                      child: widget.leadingWidget,
                    ),
                  ),
                  Text(widget.title ?? "",
                      style: const TextStyle(
                          fontSize: 17, color: Color(0xFF333333))),
                  Positioned(
                    right: 0,
                    child: Container(
                      padding: const EdgeInsets.only(right: 5),
                      child: widget.trailingWidget,
                    ),
                  ),
                ],
              ),
            )
        ),
      ),
    );
  }
}

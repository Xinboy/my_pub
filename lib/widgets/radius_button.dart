import 'package:flutter/material.dart';

/// 圆角按钮
class RadiusButton extends StatelessWidget {
  // 按钮宽高
  final double width;
  final double height;
  final Widget child;
  final BorderRadius? borderRadius;
  final Color? backgroundColor;
  //点击回调
  final GestureTapCallback onPressed;

  const RadiusButton({
    Key? key,
    this.width = 0.0,
    this.height = 0.0,
    this.borderRadius,
    required this.child,
    required this.onPressed,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          borderRadius: borderRadius,
              color: backgroundColor
      ),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          highlightColor: Colors.transparent,
          borderRadius: borderRadius,
          onTap: onPressed,
          child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(height: height, width: width),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: DefaultTextStyle(
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  child: child,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

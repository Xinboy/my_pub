import 'package:dio/dio.dart';
import 'package:my_pub/https/http_header.dart';

/// 针对每一个网络请求接口，返回对应数据
class HttpsTool {
  static Future<ApiResponse<GetScienceArticleEntity>> getScienceArticle(
      {int? pageNum}) async {
    try {
      final response =
          await Https.instance.get(kApiHome, params: {"pageNum": pageNum});
      var data = GetScienceArticleEntity.fromJson(response);
      return ApiResponse.completed(data);
    } on DioError catch (e) {
      return ApiResponse.error(e.error);
    }
  }
}

/// 测试 Model
class GetScienceArticleEntity {
  String? code;
  String? msg;
  Data? data;

  GetScienceArticleEntity({this.code, this.msg, this.data});

  GetScienceArticleEntity.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = code;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data?.toJson();
    }
    return data;
  }
}

class Data {
  int? pageCount;
  int? pageTotal;
  int? pageSize;
  List<PageList>? pageList;
  int? pageNum;

  Data(
      {this.pageCount,
      this.pageTotal,
      this.pageSize,
      this.pageList,
      this.pageNum});

  Data.fromJson(Map<String, dynamic> json) {
    pageCount = json['pageCount'];
    pageTotal = json['pageTotal'];
    pageSize = json['pageSize'];
    if (json['pageList'] != null) {
      pageList = <PageList>[];
      json['pageList'].forEach((v) {
        pageList?.add(new PageList.fromJson(v));
      });
    }
    pageNum = json['pageNum'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pageCount'] = pageCount;
    data['pageTotal'] = pageTotal;
    data['pageSize'] = pageSize;
    if (pageList != null) {
      data['pageList'] = pageList?.map((v) => v.toJson()).toList();
    }
    data['pageNum'] = pageNum;
    return data;
  }
}

class PageList {
  int? articleId;
  String? articleTitle;
  int? articleType;
  String? articleContent;
  int? articleContentType;
  int? articleStatus;
  String? articleCreateTime;
  String? articleUpdateTime;
  int? articleSort;

  PageList(
      {this.articleId,
      this.articleTitle,
      this.articleType,
      this.articleContent,
      this.articleContentType,
      this.articleStatus,
      this.articleCreateTime,
      this.articleUpdateTime,
      this.articleSort});

  PageList.fromJson(Map<String, dynamic> json) {
    articleId = json['articleId'];
    articleTitle = json['articleTitle'];
    articleType = json['articleType'];
    articleContent = json['articleContent'];
    articleContentType = json['articleContentType'];
    articleStatus = json['articleStatus'];
    articleCreateTime = json['articleCreateTime'];
    articleUpdateTime = json['articleUpdateTime'];
    articleSort = json['articleSort'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['articleId'] = articleId;
    data['articleTitle'] = articleTitle;
    data['articleType'] = articleType;
    data['articleContent'] = articleContent;
    data['articleContentType'] = articleContentType;
    data['articleStatus'] = articleStatus;
    data['articleCreateTime'] = articleCreateTime;
    data['articleUpdateTime'] = articleUpdateTime;
    data['articleSort'] = articleSort;
    return data;
  }
}

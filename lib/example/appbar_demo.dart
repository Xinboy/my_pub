import 'package:flutter/material.dart';
import 'package:my_pub/widgets/expanded_area_widget.dart';
import 'package:my_pub/widgets/opacity_appbar.dart';
import 'package:my_pub/widgets/watermark.dart';

class AppbarDemo extends StatefulWidget {
  @override
  _AppbarDemoState createState() => _AppbarDemoState();
}

class _AppbarDemoState extends State<AppbarDemo> {

  final double maxOffset = 80;
  late OpacityAppBar appbar;

  /// 通过监听 scrollview的滑动距离，计算各个部件的透明度。
  /// 在这里 把完全透明到不透明 需要滑动的距离暂定为 80（单位逻辑像素 logical pixels）
  scrollDidScrolled(double offset) {
    ///appbar 透明度
    double appBarOpacity = offset / maxOffset;

    if (appBarOpacity < 0) {
      ///默认隐藏
      appBarOpacity = 0.0;
    } else if (appBarOpacity > 1) {
      ///当滑动距离超过80后，显示 appbar
      appBarOpacity = 1.0;
    }

    print("object");

    ///更新透明度
    if (appbar.updateAppBarOpacity != null) {
      print("updateAppBarOpacity");
      appbar.updateAppBarOpacity!(appBarOpacity);
    }
  }

  OpacityAppBar _appbar() {
    appbar = OpacityAppBar(
      leadingWidget: Padding(
        padding: EdgeInsets.only(left: 15),
        child: Row(
          children:  [
            IconButton(icon: const Icon(Icons.arrow_back_ios), onPressed: (){},),
            const SizedBox(width: 10),
            const Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
      ),
      trailingWidget: IconButton(
        onPressed: () {},
        icon: const Icon(Icons.settings),
      ),
    );
    return appbar;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appbar(),
      body: Stack(
        children: [
          /// 需要用NotificationListener监听滑动滚动
          NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollUpdateNotification &&
                  notification.depth == 0) {
                //滑动通知
                scrollDidScrolled(notification.metrics.pixels);
              }
              // 通知不再上传
              return true;
            },
            child: ListView(
              children: List.generate(100, (index) {
                return Container(
                  color: Colors.red,
                  width: 100,
                  height: 40,
                  child: Text("$index"),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_pub/https/http_header.dart';
import 'package:my_pub/utils/http_utils.dart';

class HttpRoute extends StatefulWidget {
  const HttpRoute({Key? key}) : super(key: key);

  @override
  _HttpRouteState createState() => _HttpRouteState();
}

class _HttpRouteState extends State<HttpRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: const Text('请求'),
          onPressed: () {
            _request();
          },
        ),
      ),
    );
  }

  void _request() async {
    ApiResponse<GetScienceArticleEntity> res =
        await HttpsTool.getScienceArticle();
    if (res.status != Status.completed) return;
    if (kDebugMode) {
      print(res.data?.data?.pageCount);
    }
  }
}

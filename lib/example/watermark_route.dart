import 'package:flutter/material.dart';
import 'package:my_pub/widgets/expanded_area_widget.dart';
import 'package:my_pub/widgets/watermark.dart';

class WaterMarkRoute extends StatefulWidget {
  @override
  _WaterMarkRouteState createState() => _WaterMarkRouteState();
}

class _WaterMarkRouteState extends State<WaterMarkRoute> {
  int _status = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          showWaterMarkButton(),
          waterMark(),
        ],
      ),
    );
  }

  Widget showWaterMarkButton() {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 100.0, bottom: 10.0),
            child: ElevatedButton(
              child: const Text('单行文本水印'),
              onPressed: () {
                setState(() {
                  _status = 1;
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: ElevatedButton(
              child: const Text('交替文本水印'),
              onPressed: () {
                setState(() {
                  _status = 2;
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: ElevatedButton(
              child: const Text('清除水印'),
              onPressed: () {
                setState(() {
                  _status = 0;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget waterMark() {
    if (_status == 0) {
      return Container();
    } else {
      return IgnorePointer(
        child: TranslateWithExpandedPaintingArea(
          offset: const Offset(-30, 0),
          child: WaterMark(
              painter: _status == 1
                  ? TextWaterMarkPainter(
                      text: "Flutter",
                      textStyle: const TextStyle(
                        color: Colors.black38,
                      ),
                      rotate: -20.0)
                  : StaggerTextWaterMarkPainter(
                      text: "Flutter",
                      text2: "Xinbo",
                      textStyle: const TextStyle(
                        color: Colors.black38,
                      ),
                      padding2: const EdgeInsets.only(left: 40),
                      rotate: -20.0)),
        ),
      );
    }
  }
}

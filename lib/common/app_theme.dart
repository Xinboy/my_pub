import 'package:flutter/material.dart';
import 'app_color.dart';

final ThemeData themeData = ThemeData(
  //页面背景色
  scaffoldBackgroundColor: AppColors.page,
  //主题色
  primaryColor: AppColors.primary,
  //取消水波纹效果
  splashColor: Colors.transparent,
  //取消水波纹效果
  highlightColor: Colors.transparent,
  //文字未选中的颜色
  textTheme: const TextTheme(
    bodyText2: TextStyle(
      color: AppColors.un1active,
    ),
  ),
  appBarTheme: const AppBarTheme(
    //appBar底部为透明，无线条
    elevation: 0,
    color: AppColors.nav,
  ),
  //TabBarTheme选项卡的的颜色
  indicatorColor: AppColors.active,
  //TabBarTheme选项卡的样式
  tabBarTheme: const TabBarTheme(
    unselectedLabelColor: AppColors.un1active,
    indicatorSize: TabBarIndicatorSize.label,
    labelStyle: TextStyle(
      fontSize: 18,
    ),
    labelPadding: EdgeInsets.symmetric(horizontal: 12),
  ),
  //底部按钮样式
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: AppColors.nav,
    selectedItemColor: AppColors.active,
    unselectedItemColor: AppColors.un1active,
    selectedLabelStyle: TextStyle(
      fontSize: 12,
    ),
  ),
);

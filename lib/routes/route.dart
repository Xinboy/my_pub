import 'package:flutter/material.dart';



import 'package:my_pub/example/watermark_route.dart';
import '../main.dart';

import 'route_constants.dart';



///配置路由
final routes = {
  kWaterMarkRoute: (context) => WaterMarkRoute(),
//  '/counter': (context) => CounterWidget(),
//  '/widget_state_A': (context) => TapBoxA(),
//  '/widget_state_B': (context) => ParentWidget(),
//  '/widget_state_C': (context) => ParentWidgetC(),
//  '/router_A': (context, {arguments}) => RouterA(arguments: arguments),
//  '/textfield': (context) => FocusTestRouter(),
//  '/form': (context) => FormTestRoute(),
//  '/progress': (context) => ProgressRoute(),

};

///固定写法
// ignore: prefer_function_declarations_over_variables
var onGenerateRoute = (RouteSettings settings) {
  final String name = settings.name!;
  final Function pageContentBuilder = routes[name]!;
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, argument: settings.arguments));
      return route;
    } else {
      final Route route =
      MaterialPageRoute(builder: (context) => pageContentBuilder(context));
      return route;
    }
  }
  return null;
};

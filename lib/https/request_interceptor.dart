part of https;

/// 请求处理拦截器
class RequestInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // TODO: implement onRequest
    debugPrint('''======================\n
        *** Request *** \n
        Data:\n 
        ${options.data.toString()} \n
        Query:\n 
        ${options.queryParameters.toString()} \n
        ======================''');

    /// 设置cookie
    // var cookie = SpUtil.getStringList('cookie');

    // if (options.path != 'api/auth/login' &&
    //     cookie != null &&
    //     cookie.length > 0) {
    //   options.headers['cookie'] = cookie;//这里就是除了登录的情况其他都加cookie
    // }
    // options.headers['User-Agent'] = 'gzzoc-1';//
    //
    // if (options.data?.runtimeType == FormData) {
    //   options.headers['content-type'] = 'multipart/form-data';//FormData这种情况改content-type
    // }

    // // 加载动画----这个就是请求页面时的那个loading窗 --处理逻辑 我是用options?.data['showLoading']或options?.queryParameters['showLoading'],
    //就是我们在传参的时候多加一个参数，这个因为前人就这样做的，也跟后端约定的，后端见showLoading不做处理。这样不是很好，反正options是有其他字段加的
    // if (options?.data?.runtimeType == FormData) {
    //   Alert.showLoading();
    // } else if ((options?.data != null &&
    //         false == options?.data['showLoading']) ||
    //     (options?.queryParameters != null &&
    //         false == options?.queryParameters['showLoading'])) {
    //   // 不显示加载动画
    // } else {
    //   Alert.showLoading();
    // }

    ///在这做请求时显不显示Loading的处理

    handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    // TODO: implement onResponse
    super.onResponse(response, handler);

    debugPrint(''''======================\n
        *** Response *** \n
        ${response.toString()}''');


    if (response.data != null &&
        response.data is Map &&
        response.data['code'] == '0') {
      /// 请求成功的结果；
      /// code值需要根据实际项目接口文档进行判断，当前值为 0

      /// 登录请求，需要额外存储缓存cookie
      if (response.requestOptions.path == kApiLogin) {
        // 缓存cookie
        var cookie = response.headers['set-cookie'];
        SpUtils.setStringList('cookie', cookie!);
      }

      handler.next(response);
    } else if (response.requestOptions.path == kApiLogin &&
        response.data != null &&
        response.data is Map &&
        response.data['code'] == '11') {
      /// 登录成功，
      /// code值需要根据实际项目接口文档进行判断，当前值为 11
      /// !!! 根据项目需求觉得是否需要该判断分支
      // 缓存cookie
      var cookie = response.headers['set-cookie'];
      SpUtils.setStringList('cookie', cookie!);

      handler.next(response);
    } else {
      /// 请求失败的结果
      /// error json错误内容；需要根据实际项目接口文档进行判断
      handler.reject(DioError(
        requestOptions: response.requestOptions,
        error: response.data != null &&
                response.data is Map &&
                response.data['msg'] != null &&
                response.data['msg'].length > 0
            ? response.data['msg']
            : '未知错误',
      ));
    }
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    // TODO: implement onError
    // Alert.hide();关闭弹窗

    /// 账户登录异常
    if (err.response != null &&
        err.response?.data != null &&
        err.response?.data is Map &&
        err.response?.data != null &&
        err.response?.data['code'] == '2') {
      /// code值需要根据实际项目接口文档进行判断，当前值为 2

      /// 登录异常处理
    } else {
      ///在页面显示一个错误弹窗
    }
    AppException appException = AppException.create(err);
    err.error = appException;
    return super.onError(err, handler);
  }
}

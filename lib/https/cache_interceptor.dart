part of https;

/// 网络请求 网络缓存
class CacheObject {
  CacheObject(this.response)
      : timeStamp = DateTime.now().millisecondsSinceEpoch;

  Response response;
  // 缓存创建时间
  int timeStamp;

  @override
  bool operator ==(other) {
    return response.hashCode == other.hashCode;
  }

  //将请求uri作为缓存的key
  @override
  // TODO: implement hashCode
  int get hashCode => response.realUri.hashCode;
}

class NetCacheInterceptor extends Interceptor {
  // 为确保迭代器顺序和对象插入时间一致顺序一致，我们使用LinkedHashMap
  var cache = LinkedHashMap<String, CacheObject>();

  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    // TODO: implement onRequest
    super.onRequest(options, handler);

    if (!kCacheEnable) handler.next(options);

    // refresh标记是否是"下拉刷新"
    bool refresh = options.extra["refresh"] == true;
    // 是否磁盘缓存
    bool cacheDisk = options.extra["cacheDisk"] == true;

    if (refresh) {
      if (options.extra["list"] == true) {
        //若是列表，则只要url中包含当前path的缓存全部删除（简单实现，并不精准）
        cache.removeWhere((key, value) => key.contains(options.path));
      } else {
        // 如果不是列表，则只删除uri相同的缓存
        delete(options.uri.toString());
      }
      handler.next(options);
    }

    // get 请求，开启缓存
    if (options.extra["noCache"] != true &&
        options.method.toLowerCase() == 'get') {
      String key = options.extra["cacheKey"] ?? options.uri.toString();

      // 策略 1 内存缓存优先，2 然后才是磁盘缓存
      var ob = cache[key];
      if (ob != null) {
        if ((DateTime.now().millisecondsSinceEpoch - ob.timeStamp) / 1000 <
            kCacheMaxAge) {
          handler.resolve(cache[key]!.response);
        } else {
          cache.remove(key);
        }
      }

      if (cacheDisk) {
        var cacheData = SpUtils.getJson(key);
        if (cacheData != null) {
          handler.resolve(Response(
              requestOptions: options, statusCode: 200, data: cacheData));
        }
      }
    }
  }


  @override
  Future<void> onResponse(Response response, ResponseInterceptorHandler handler) async {
    // TODO: implement onResponse
    super.onResponse(response, handler);
    // 如果启用缓存，将返回结果保存到缓存
    if (kCacheEnable) {
      await _saveCache(response);
    }
  }

  Future<void> _saveCache(Response response) async {
    RequestOptions options = response.requestOptions;

    // 只缓存 get 的请求
    if (options.extra["noCache"] != true &&
        options.method.toLowerCase() == 'get') {

      // 策略：内存、磁盘都写缓存
      String key = options.extra['cacheKey'] ?? options.uri.toString();


      // 磁盘缓存
      if (options.extra["cacheDisk"] == true) {
        await SpUtils.setJson(key, response.data);
      }

      // 内存缓存
      // 如果缓存数量超过最大数量限制，则先移除最早的一条记录
      if (cache.length == kCacheMaxCount) {
        cache.remove(cache[cache.keys.first]);
      }

      cache[key] = CacheObject(response);
    }
  }

  void delete(String key) {
    cache.remove(key);
  }
}

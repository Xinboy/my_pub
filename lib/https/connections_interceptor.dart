part of https;

/// 网络请求 网络拦截器
class ConnectsInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // TODO: implement onRequest
    super.onRequest(options, handler);
    _request();
  }

  _request() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    switch (connectivityResult) {
      case ConnectivityResult.mobile:
        {
          // I am connected to a mobile network.
        }
        break;
      case ConnectivityResult.wifi:
        {
          // I am connected to a wifi network.
        }
        break;
      case ConnectivityResult.none:
        {
          // none network.
        }
        break;
    }
  }
}

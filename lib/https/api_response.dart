part of https;

enum Status { completed, error }

class ApiResponse<T> implements Exception {
  late Status status;
  T? data;
  AppException? exception;
  ApiResponse.completed(this.data) : status = Status.completed;
  ApiResponse.error(this.exception) : status = Status.error;

  @override
  String toString() {
    // TODO: implement toString
    return "Status : $status \n Message : $exception \n Data : $data";
  }
}

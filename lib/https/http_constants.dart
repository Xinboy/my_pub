part of https;

// ignore: slash_for_doc_comments
/**************** 网络请求 Api *****************/

const String kBaseUrl = 'http://baidu.com/';
const String kApiLogin = 'customer/v2/signin';
const String kApiHome = 'customer/v2/home';

/**************** 启用代理 *****************/
/// 是否启用代理
const kProxyEnable = false;
/// 代理服务IP
const kProxyIp = '192.168.1.210';
/// 代理服务端口
const kProxyPort = 8080;


/// 是否开启网络缓存
const bool kCacheEnable = true;
/// 网络缓存最长时间
const int kCacheMaxAge = 3600;
/// 网络缓存最大数量
const int kCacheMaxCount = 100;
